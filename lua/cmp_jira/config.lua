local M = {
    filetypes = { 'gitcommit' },
    jira = {
        url = '',
        email = '',
        jql = 'assignee="%s"+AND+resolution=unresolved+order+by+updated+DESC',
    },
}

return M
